# Introduction
This documentation will include all the documentation from start till the end of febzreo course.
The trainer of the Feb Zero are Mr.Francisco Sanchezz from Barcelona, Spain and Mr.Sibu from Karala India. With the community case detected the Thimphu is under lockdown so, we are taking the feb zero course online and so is the two instructor the Franciso is at the quarantine  and Sibu from home, we hope the lockdown will lift on feb 6 2022.

## Quick Negivation to the steps to follow for software installation and a brief intro.
- [WSL installation](#editing-this-readme)!
- [Visual Studio code installation](#editing-this-readme)!
- [Git installation](#editing-this-readme)!
- [Intalling imagemagick](#editing-this-readme)!
- [Installing youtube-dl](#editing-this-readme)!
- [Installing KiCAD](#editing-this-readme)!
- [Installing Kdenlive (video editing)](#editing-this-readme)!
- [Installing Gimp (image editing)](#editing-this-readme)!
- [Installing inkspace(vector image editing)](#editing-this-readme)!
- [Installing Freecad(3D CAD)](#editing-this-readme)!
- [Installing openscad (programming 3D cad)](#editing-this-readme)!
- [handbreak (video transcoder)](#editing-this-readme)!

# WSL installation

- [ ] Windows Subsystem for Linux is a compatibility layer for running Linux binary executables natively on Windows 10, Windows 11, and Windows Server 2019. [WSL install](https://docs.microsoft.com/en-us/windows/wsl/install), alternatively you can also use the Vartual Machine (VM) using [Virtual box](https://www.virtualbox.org/) and follow the VM installation process to [this link](https://www.youtube.com/watch?v=wX75Z-4MEoM)

- Running the Ubuntu as VM in windows has response issue while using browsers.
  
- [ ] Installing the WSL with the following command:

``` 
wsl --install
sudo apt update 
sudo apt upgrade 
```

# Visual Studio code 
- [ ] Navigate to the [link](https://code.visualstudio.com/download) to dowanload the .exe file for your operating system.
- [ ] Follow the steps by runing the .exe file.

# Git Installation
- [ ] open the Ubuntu command terminal and execute the following command

```
sudo apt install git
cd ~/folder_name # replace this with your repository folder
git config --global user.name "yourname" #set name for commits
git config --global user.email "youremail" #set email for commits
git config --global push.default simple #default push only the current branch
git config --global core.editor nano #set the editor
```

- [ ] [Generating a SSH Keypair](https://docs.github.com/es/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) 
- [ ] After creating the ssh keypair, and adding it to the ssh-agent, you have to upload the public key to github/gitlab. For copying the contents of the public key to the clipboard you can just open it in any text editor, select all of it contents and copy it. I will use a command line tool called cat. Say our public key name is id_rsa.pub, to display its contents we do

```
cat ~/.ssh/id_rsa.pub
```
- [ ] And it is just a matter copying and pasting in the SSH Keys section of github/gitlab. A piece of advice. Name that key in github/gitlab as the computer you are using. If you loose that computer or you feel that the key is compromised you will know what key to delete.
- [ ]  Cloning a repository using a SSH connection, This time locate the SSH address to clone the repository. In your computer, inside a terminal window, navigate to where you want to clone the repository (recommended your home directory):
```
cd  # this goes to home folder
git clone paste-the-address-here.git
```
- [ ] Basic git workflow,
this is the basic git workflow. Once you have made all changes to your website (hopefully daily), upload those changes to the repository. Very important: Do not miss any step, and do them in order.
```
cd ~/repositoryfolder     # go to the local repository folder
du -sh personalsubfolder  # check your subfolder size
git pull                  # pull other students changes
git add --all             # add your changes
git commit -m "message"   # write a meaningful message
git push                  # push to the archive
```
![i](Image/im.jpg "image") 
